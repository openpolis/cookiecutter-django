TERMINATOR = "\x1b[0m"
WARNING = "\x1b[1;33m [WARNING]: "
INFO = "\x1b[1;33m [INFO]: "
HINT = "\x1b[3;33m"
SUCCESS = "\x1b[1;32m [SUCCESS]: "

project_module = '{{ cookiecutter.project_module }}'
if hasattr(project_module, 'isidentifier'):
    assert project_module.isidentifier(), "'{}' project slug is not a valid Python identifier.".format(project_module)

assert "\\" not in "{{ cookiecutter.author_name }}", "Don't include backslashes in author name."
