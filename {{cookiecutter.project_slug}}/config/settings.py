# -*- coding: utf-8 -*-
"""
Django settings for {{ cookiecutter.project_name }} project.

For more information on this file, see
https://docs.djangoproject.com/en/dev/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/dev/ref/settings/
"""
import os
from pathlib import Path

import environ
import corsheaders

# PATH CONFIGURATION
# -----------------------------------------------------------------------------
# Build paths inside the project like this: os.path.join(ROOT_PATH, ...)
ROOT_PATH = environ.Path(__file__) - 2  # ROOT/project/settings.py - 2 = ROOT/
PROJECT_NAME = '{{cookiecutter.project_slug}}'
PROJECT_PACKAGE = 'project'
PROJECT_PATH = ROOT_PATH.path(PROJECT_PACKAGE)
APPS_PATH = PROJECT_PATH
CONFIG_PATH = ROOT_PATH.path('config')
RESOURCES_PATH = ROOT_PATH.path('resources')
LOCALE_PATHS = [
    str(ROOT_PATH.path('locale')),
]

# Add our project to our python path, this way we don't need to type our project
# name in our dotted import paths:
# os.path.append(PROJECT_PATH)

# READ ENVIRON
# -----------------------------------------------------------------------------
env = environ.Env()
# .env file, should load only in development environment
# Operating System Environment variables have precedence over variables defined
# in the .env file, that is to say variables from the .env files will only be
# used if not defined as environment variables.
READ_DOT_ENV_FILE = env.bool('DJANGO_READ_DOT_ENV_FILE', default=True)
if READ_DOT_ENV_FILE:
    dot_env_path = str(CONFIG_PATH.path('.env'))
    dot_env_file = Path(dot_env_path)
    if dot_env_file.is_file():
        env.read_env(str(dot_env_file))

# GENERAL
# -----------------------------------------------------------------------------
# https://docs.djangoproject.com/en/dev/ref/settings/#debug
DEBUG = env.bool('DEBUG', False)
# Local time zone. Choices are
# http://en.wikipedia.org/wiki/List_of_tz_zones_by_name
# though not all of them may be available with every OS.
# In Windows, this must be set to your system time zone.
TIME_ZONE = '{{ cookiecutter.timezone }}'
# https://docs.djangoproject.com/en/dev/ref/settings/#language-code
LANGUAGE_CODE = '{{ cookiecutter.language_code }}'
# https://docs.djangoproject.com/en/dev/ref/settings/#site-id
SITE_ID = 1
# https://docs.djangoproject.com/en/dev/ref/settings/#use-i18n
USE_I18N = True
# https://docs.djangoproject.com/en/dev/ref/settings/#use-l10n
USE_L10N = True
# https://docs.djangoproject.com/en/dev/ref/settings/#use-tz
USE_TZ = True

USE_THOUSAND_SEPARATOR = True


# MANAGER CONFIGURATION
# -----------------------------------------------------------------------------
ADMIN_EMAIL = env('ADMIN_EMAIL', default='admin@{project_name}.com'.format(project_name=PROJECT_NAME))
ADMIN_NAME = env('ADMIN_NAME', default=ADMIN_EMAIL.split('@')[0])

# See: https://docs.djangoproject.com/en/dev/ref/settings/#admins
ADMINS = (
    (ADMIN_EMAIL, ADMIN_NAME),
)

# See: https://docs.djangoproject.com/en/dev/ref/settings/#managers
MANAGERS = ADMINS

# See: https://docs.djangoproject.com/en/dev/ref/settings/#default-from-email
DEFAULT_FROM_EMAIL = env('DEFAULT_FROM_EMAIL', default=ADMIN_EMAIL)

# DATABASES
# ------------------------------------------------------------------------------
# https://docs.djangoproject.com/en/dev/ref/settings/#databases
{% if cookiecutter.use_docker == 'y' -%}
DATABASES = {
    'default': env.db('DATABASE_URL'),
}
{%- else %}
DATABASES = {
    'default': env.db('DATABASE_URL', default='postgres://{% if cookiecutter.windows == 'y' %}localhost{% endif %}/{{cookiecutter.project_slug}}'),
}
{%- endif %}
DATABASES['default']['ATOMIC_REQUESTS'] = True

# MEDIA CONFIGURATION
# -----------------------------------------------------------------------------
# See: https://docs.djangoproject.com/en/dev/ref/settings/#media-root
MEDIA_ROOT = env('MEDIA_ROOT', default=str(RESOURCES_PATH.path('media')))
# See: https://docs.djangoproject.com/en/dev/ref/settings/#media-url
MEDIA_URL = '/media/'

# STATIC FILE CONFIGURATION
# -----------------------------------------------------------------------------
# See: https://docs.djangoproject.com/en/dev/ref/settings/#static-root
STATIC_ROOT = env('STATIC_ROOT', default=str(RESOURCES_PATH.path('static')))
# See: https://docs.djangoproject.com/en/dev/ref/settings/#static-url
STATIC_URL = '/static/'
# See: https://docs.djangoproject.com/en/dev/ref/settings/#std:setting-STATICFILES_DIRS
STATICFILES_DIRS = ()
# See: https://docs.djangoproject.com/en/dev/ref/contrib/staticfiles/#staticfiles-finders
STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
)

# SECRET CONFIGURATION
# -----------------------------------------------------------------------------
# See: https://docs.djangoproject.com/en/dev/ref/settings/#secret-key
# SECURITY WARNING: keep the secret key used in production secret!
# Note: This not-so-secret-key should only be used for development and testing.
SECRET_KEY = env('SECRET_KEY', default='not-so-secret')

# SITE CONFIGURATION
# -----------------------------------------------------------------------------
# Hosts/domain names that are valid for this site
# See https://docs.djangoproject.com/en/1.11/ref/settings/#allowed-hosts
ALLOWED_HOSTS = env.list('ALLOWED_HOSTS', default='*')

# FIXTURE CONFIGURATION
# -----------------------------------------------------------------------------
# See: https://docs.djangoproject.com/en/dev/ref/settings/#std:setting-FIXTURE_DIRS
FIXTURE_DIRS = (str(RESOURCES_PATH.path('fixtures')),)

# TEMPLATE CONFIGURATION
# -----------------------------------------------------------------------------
TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [os.path.join(str(PROJECT_PATH), 'templates')],
        'APP_DIRS': True,
        'OPTIONS': {
            'debug': DEBUG,
            'context_processors': [
                # Insert your TEMPLATE_CONTEXT_PROCESSORS here or use this
                # list if you haven't customized them:
                'django.contrib.auth.context_processors.auth',
                'django.template.context_processors.debug',
                'django.template.context_processors.i18n',
                'django.template.context_processors.media',
                'django.template.context_processors.static',
                'django.template.context_processors.tz',
                'django.contrib.messages.context_processors.messages',
                'django.template.context_processors.request',
            ],
        },
    },
]

# MIDDLEWARE CONFIGURATION
# -----------------------------------------------------------------------------
# See: https://docs.djangoproject.com/en/dev/ref/settings/#middleware-classes
MIDDLEWARE = [
    'corsheaders.middleware.CorsMiddleware',
    {%- if cookiecutter.amp_support == 'y' -%}
    'config.middleware.AMPMiddleware',
    {%- endif %}
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
]

# URL CONFIGURATION
# -----------------------------------------------------------------------------
# See: https://docs.djangoproject.com/en/dev/ref/settings/#root-urlconf
ROOT_URLCONF = 'config.urls'

# APPS CONFIGURATION
# -----------------------------------------------------------------------------
DJANGO_APPS = (
    # Default Django apps:
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.sites',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.gis',
    # Django helper
    'django_extensions',
    'corsheaders',
    {%- if cookiecutter.use_django_rest_framework == 'y' -%}
    # Rest framework
    'rest_framework',
    {%- endif %}
)

# Apps specific for this project go here.
LOCAL_APPS = (
    # 'project.testapp',
)

# See: https://docs.djangoproject.com/en/dev/ref/settings/#installed-apps
INSTALLED_APPS = DJANGO_APPS + LOCAL_APPS

# AUTHENTICATION CONFIGURATION
# -----------------------------------------------------------------------------
AUTHENTICATION_BACKENDS = (
    'django.contrib.auth.backends.ModelBackend',
)

# LOGGING CONFIGURATION
# -----------------------------------------------------------------------------
# See: https://docs.djangoproject.com/en/dev/ref/settings/#logging
# See http://docs.djangoproject.com/en/dev/topics/logging for
# more details on how to customize your logging configuration.
LOGS_PATH = env(
    'LOGS_PATH',
    default=os.path.normpath(str(RESOURCES_PATH) + "/logs")
)

LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'verbose': {
            'format': '[%(asctime)s] %(levelname)s [%(name)s:%(lineno)s] %(message)s',
            'datefmt': '%d/%b/%Y %H:%M:%S'
        },
        'simple': {
            'format': '%(levelname)s %(message)s'
        },
    },
    'handlers': {
        'file': {
            'level': 'INFO',
            'class': 'logging.handlers.RotatingFileHandler',
            'filename': os.path.normpath(
                os.path.join(LOGS_PATH, 'operations.log')
            ),
            'maxBytes': 1024 * 1024 * 10,  # 10 MB
            'backupCount': 7,
            'formatter': 'verbose',
        },
        'console': {
            'level': 'DEBUG',
            'class': 'logging.StreamHandler',
            'formatter': 'verbose',
        }
    },
    'loggers': {
        'django': {
            'handlers': [],
            # 'handlers': ['file', 'console'],
            'level': 'DEBUG',
            'propagate': True,
        },
        '{{ cookiecutter.project_module }}': {
            'handlers': ['file', 'console'],
            'level': 'INFO',
            'propagate': True,
        }
    },
}

# TOOLBAR CONFIGURATION
# -----------------------------------------------------------------------------
# See: http://django-debug-toolbar.readthedocs.org/en/latest/installation.html#explicit-setup
DEBUG_TOOLBAR = env.bool('DEBUG_TOOLBAR', DEBUG)
if DEBUG_TOOLBAR:
    INSTALLED_APPS += (
        'debug_toolbar',
    )
    MIDDLEWARE += (
        'debug_toolbar.middleware.DebugToolbarMiddleware',
    )
    DEBUG_TOOLBAR_PATCH_SETTINGS = False

    # http://django-debug-toolbar.readthedocs.org/en/latest/installation.html
    INTERNAL_IPS = env(
        'DEBUG_TOOLBAR_INTERNAL_IPS',
        default='127.0.0.1'
    ).split(',')

DEBUG_JSON_AS_HTML = env('DEBUG_JSON_AS_HTML', default=False)
if DEBUG_JSON_AS_HTML:
    MIDDLEWARE += (
        'config.middleware.JsonAsHtml',
    )

# WSGI CONFIGURATION
# -----------------------------------------------------------------------------
# See: https://docs.djangoproject.com/en/dev/ref/settings/#wsgi-application
WSGI_APPLICATION = 'wsgi.application'
# END WSGI CONFIGURATION


# TESTING CONFIGURATION
# -----------------------------------------------------------------------------
# See: https://docs.djangoproject.com/en/dev/releases/1.6/#new-test-runner
TEST_RUNNER = 'django.test.runner.DiscoverRunner'

# CACHES CONFIGURATION
# -----------------------------------------------------------------------------
CACHES = {
    'default': {
        'BACKEND': 'django.core.cache.backends.db.DatabaseCache',
        'LOCATION': 'the_cache_table',
    }
}

# PASSWORD VALIDATION CONFIGURATION
# -----------------------------------------------------------------------------
# https://docs.djangoproject.com/en/1.11/ref/settings/#auth-password-validators
AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]

{%- if cookiecutter.use_django_rest_framework == 'y' %}
# REST FRAMEWORK CONFIGURATION
# -----------------------------------------------------------------------------
REST_FRAMEWORK = {
    # Only authenticated users can access the API
    # Authentication may be passed through:
    # - a cookie,
    # - a basic_auth header
    # - a jwt header (json web token)
    'DEFAULT_PERMISSION_CLASSES': [
        'rest_framework.permissions.IsAuthenticatedOrReadOnly',
    ],
    'DEFAULT_AUTHENTICATION_CLASSES': (
        'rest_framework_jwt.authentication.JSONWebTokenAuthentication',
        'rest_framework.authentication.SessionAuthentication',
        'rest_framework.authentication.BasicAuthentication',
    ),
    'DEFAULT_FILTER_BACKENDS': (
        'django_filters.rest_framework.DjangoFilterBackend',
    ),
    'DEFAULT_RENDERER_CLASSES': (
        'rest_framework.renderers.JSONRenderer',
        'rest_framework.renderers.BrowsableAPIRenderer',
        'rest_framework.renderers.AdminRenderer',
    ),
    'DEFAULT_VERSIONING_CLASS': 'rest_framework.versioning.URLPathVersioning',
    'DEFAULT_VERSION': 'v1',
    'DEFAULT_THROTTLE_CLASSES': (
        'rest_framework.throttling.AnonRateThrottle',
    ),
    'DEFAULT_THROTTLE_RATES': {
        'anon': '10000/day',
    }
}

# JWT AUTH CONFIGURATION
# -----------------------------------------------------------------------------
# See: http://getblimp.github.io/django-rest-framework-jwt
JWT_AUTH = {
    'JWT_SECRET_KEY': SECRET_KEY,
    'JWT_GET_USER_SECRET_KEY': None,
    'JWT_PUBLIC_KEY': None,
    'JWT_PRIVATE_KEY': None,
    'JWT_ALGORITHM': 'HS256',
    'JWT_VERIFY': True,
    'JWT_VERIFY_EXPIRATION': True,
    'JWT_LEEWAY': 0,
    'JWT_EXPIRATION_DELTA': datetime.timedelta(days=1),
    'JWT_AUDIENCE': None,
    'JWT_ISSUER': None,

    'JWT_ALLOW_REFRESH': True,
    'JWT_REFRESH_EXPIRATION_DELTA': datetime.timedelta(days=7),

    'JWT_AUTH_HEADER_PREFIX': 'JWT',
    'JWT_AUTH_COOKIE': None,
}
{%- endif %}
# CORS HEADERS CONFIGURATION
# -----------------------------------------------------------------------------
if env.bool('DJANGO_CORS_HEADERS', default=False):
    MIDDLEWARE = ['corsheaders.middleware.CorsMiddleware'] + MIDDLEWARE
    DJANGO_APPS = DJANGO_APPS + ('corsheaders',)
    CORS_ORIGIN_WHITELIST = env.list('CORS_ORIGIN_WHITELIST', default=[])
    CORS_ALLOW_CREDENTIALS = env.bool('CORS_ALLOW_CREDENTIALS', default=False)
    CORS_ORIGIN_ALLOW_ALL = False
    CORS_ALLOW_CREDENTIALS = True
    CORS_ALLOW_METHODS = corsheaders.defaults.default_methods

    CORS_ALLOW_HEADERS = corsheaders.defaults.default_headers
    {%- if cookiecutter.amp_support == 'y' %}
    CORS_ALLOW_HEADERS += [
        'AMP-Access-Control-Allow-Source-Origin',
    ]
    CORS_EXPOSE_HEADERS = [
        'AMP-Access-Control-Allow-Source-Origin',
    ]
AMP_ACCESS_CONTROL_ALLOW_SOURCE_ORIGIN = ""
{%- endif %}

# OTHER CONFIGURATION
# -----------------------------------------------------------------------------
FILE_UPLOAD_PERMISSIONS = 0o644

# -----------------------------------------------------------------------------
CONTEXT = env('CONTEXT', default='production')
VERSION = __import__(PROJECT_PACKAGE).__version__
# Usually set by the Gitlab runner, to set it manually:
# CI_COMMIT_SHA=$(git rev-parse HEAD)
GIT_REVISION = env.str('CI_COMMIT_SHA', default=None)
