Cookiecutter Django
===================

Powered by Cookiecutter_, Cookiecutter Django is a framework for jumpstarting
production-ready Django projects quickly.

* Based off and largely inspired by `pydanny's Cookiecutter Django`_
* If you have problems with this Cookiecutter template, please open issues_ don't send emails to the maintainers.

.. _cookiecutter: https://github.com/audreyr/cookiecutter
.. _`pydanny's Cookiecutter Django`: https://github.com/pydanny
.. _issues: https://gitlab.depp.it/openpolis/cookiecutter-django/issues/new

Features
---------

* For Django 2.0
* Works with Python 3.6
* Renders Django projects with 100% starting test coverage
* 12-Factor_ based settings via django-environ_
* Docker support using docker-compose_

.. _12-Factor: http://12factor.net/
.. _django-environ: https://github.com/joke2k/django-environ
.. _docker-compose: https://github.com/docker/compose

Constraints
-----------

* Only maintained 3rd party libraries are used.
* Uses PostgreSQL everywhere 
* Environment variables for configuration (This won't work with Apache/mod_wsgi except on AWS ELB).

Usage
-----

Let's pretend you want to create a Django project called "redditclone". Rather than using ``startproject`` and then editing the results to include your name, email, and various configuration issues that always get forgotten until the worst possible moment, get cookiecutter_ to do all the work.

First, get Cookiecutter. Trust me, it's awesome::

    $ pip install "cookiecutter>=1.4.0"

Now run it against this repo::

    $ cookiecutter https://github.com/pydanny/cookiecutter-django

You'll be prompted for some values. Provide them, then a Django project will be created for you.

Enter the project and take a look around::

    $ cd projectname/
    $ ls

Create a git repo and push it there::

    $ git init
    $ git add .
    $ git commit -m "first awesome commit"
    $ git remote add origin git@gitlab.com:openpolis/projectname.git
    $ git push -u origin master

Now take a look at your repo. Don't forget to carefully look at the generated README. Awesome, right?

Articles
--------

* `Deploying Cookiecutter-Django with Docker-Compose`_ - Oct. 19, 2017
* `Using Cookiecutter to Jumpstart a Django Project on Windows with PyCharm`_ - May 19, 2017
* `Exploring with Cookiecutter`_ - Dec. 3, 2016
* `Introduction to Cookiecutter-Django`_ - Feb. 19, 2016
* `Django and GitLab - Running Continuous Integration and tests with your FREE account`_ - May. 11, 2016
* `Development and Deployment of Cookiecutter-Django on Fedora`_ - Jan. 18, 2016
* `Development and Deployment of Cookiecutter-Django via Docker`_ - Dec. 29, 2015
* `How to create a Django Application using Cookiecutter and Django 1.8`_ - Sept. 12, 2015

.. _`Deploying Cookiecutter-Django with Docker-Compose`: http://adamantine.me/2017/10/19/deploying-cookiecutter-django-with-docker-compose/
.. _`Exploring with Cookiecutter`: http://www.snowboardingcoder.com/django/2016/12/03/exploring-with-cookiecutter/
.. _`Using Cookiecutter to Jumpstart a Django Project on Windows with PyCharm`: https://joshuahunter.com/posts/using-cookiecutter-to-jumpstart-a-django-project-on-windows-with-pycharm/
.. _`Development and Deployment of Cookiecutter-Django via Docker`: https://realpython.com/blog/python/development-and-deployment-of-cookiecutter-django-via-docker/
.. _`Development and Deployment of Cookiecutter-Django on Fedora`: https://realpython.com/blog/python/development-and-deployment-of-cookiecutter-django-on-fedora/
.. _`How to create a Django Application using Cookiecutter and Django 1.8`: https://www.swapps.io/blog/how-to-create-a-django-application-using-cookiecutter-and-django-1-8/
.. _`Introduction to Cookiecutter-Django`: http://krzysztofzuraw.com/blog/2016/django-cookiecutter.html
.. _`Django and GitLab - Running Continuous Integration and tests with your FREE account`: http://dezoito.github.io/2016/05/11/django-gitlab-continuous-integration-phantomjs.html

