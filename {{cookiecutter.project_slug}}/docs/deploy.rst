.. _deploy:

Deploy
========

Intro
-----
The application stack is vertically self-contained on a single host.

The stack is defined in ``docker-compose.yml`` file.
It uses:

  - a django app (web)
  - a no-service django container for tasks
  - a frontend web server (nginx)
  - a storage geo-enabled RDBMS (postgis)

.. _local_deploy:


.. _remote_deploy:

Remote deploy with docker-machine
---------------------------------

The stack can be deployed on a remotely created machine,
using ``docker-machine`` and ``docker-compose``.

The remote machine must already have been created and provisioned.
If not, then:

.. code-block:: bash

    # set environment variable
    export TOKEN=[DIGITAL_OCEAN_TOKEN]

    # generazione di una docker-machine su digital ocean
    doctl compute droplet create \
      --enable-monitoring \
      --enable-private-networking \
      --image ubuntu-16-04-x64 \
      --region fra1 \
      --size s-2vcpu-4gb \
      --ssh-keys 99:c6:71:66:39:df:bc:15:ed:2e:7c:61:96:28:ca:ec  \
      --tag-names openpolis,depp,staging \
      --wait \
      op-staging-1

    IP da doctl droplet list (riga di op-staging-1)

    docker-machine create \
      --driver generic \
      --generic-ip-address=$IP \
      --generic-ssh-key ~/.ssh/id_rsa \
      op-staging-1


A connection to the remote machine must then be made, so that
``docker-machine`` will operate on that server, instead of locally:

.. code-block:: bash

    eval $(docker-machine env op-staging-1)



The image must be built:

.. code-block:: bash

    docker build -t registry.gitlab.depp.it/openpolis/op-politiche-2018:latest .


Finally, the stack can be created, after having set all environment variables
required by ``docker-compose``.

.. code-block:: bash

    export POSTGRES_DB=[DB_NAME]
    export POSTGRES_USER=[DB_USER]
    export POSTGRES_PASS=[DB_PASSWORD]
    export ALLOWED_HOSTS=*
    export ADMIN_NAME=[admin]
    export ADMIN_EMAIL=[me@openpolis.it]
    export GOOGLE_MAPS_API_KEY=[KEY]

    # deploy
    docker network create proxy
    dc up -d postgres web nginx
